WINDOWS UNFUCK TIPS
===================

Software Alternatives
=====================
Start Menu
----------
`StartAllBack <https://startallback.com/>`_ 

`Start11 <https://www.stardock.com/products/start11/>`_

Defrag
------
`Better Defrag (defraggler) <https://www.ccleaner.com/defraggler>`_

Widgets
-------
`Better Clock (ElevenClock) <https://github.com/martinet101/ElevenClock>`_

`Weather bar <https://weatherbarapp.com/>`_

File Managers
-------------
`One Commander <https://onecommander.com/>`_ 

`Files <https://github.com/files-community/Files>`_ 

`Total Commander <https://www.ghisler.com/>`_

`Midnight Commander <https://sourceforge.net/projects/mcwin32/>`_

Image Capture
-------------
`ShareX <https://getsharex.com/>`_ 

`Lightshot <https://app.prntscr.com/en/index.html>`_

Image Viewers
-------------
`Image viewer (irfanview) <https://www.irfanview.com/>`_

Video Players
-------------
`VLC <https://www.videolan.org/vlc/>`_ 

`mpv <https://mpv.io/>`_

Compression Tools
-----------------
`7zip <https://www.7-zip.org/>`_ 

`Peazip <https://peazip.github.io/>`_

File Transfer
-------------
`Filezilla <https://filezilla-project.org/>`_ 

`WinSCP <https://winscp.net/>`_

Multi-Monitor Utility
---------------------
`DisplayFusion <https://www.displayfusion.com/>`_ 

`DualMonitorTool <https://sourceforge.net/projects/dualmonitortool>`_

KVM
---
`Synergy <https://symless.com/synergy>`_ 

`Barrier <https://github.com/debauchee/barrier>`_

PDF Readers
-----------
`SumatraPDF <https://www.sumatrapdfreader.org/free-pdf-reader>`_ 

`Fox-IT <https://www.foxit.com/>`_ 

`Perfect PDF <https://soft-xpansion.com/pdf-apps/>`_ 

`CutePDF <https://www.cutepdf.com/index.htm>`_

Image Editors
-------------
`Paint .NET <https://www.getpaint.net/index.html>`_ 

`Gimp <https://www.gimp.org/>`_ 

`Krita <https://krita.org/en/>`_

Password Managers
-----------------
`KeepassXC <https://keepassxc.org/>`_ 

`Or check for a keepass database compatible for your taste <https://keepass.info/download.html>`_

P2P
---
`Transmission <https://transmissionbt.com/>`_ 

`QBitorrent <https://www.qbittorrent.org/>`_

Text Editors
------------
`Notepad++ <https://notepad-plus-plus.org/>`_

Note taking
-----------
`Note taking application (notion) <https://www.notion.so/>`_

Music organizers
----------------
`Strawberry <https://www.strawberrymusicplayer.org/>`_ 

`foobar2000 <https://www.foobar2000.org/>`_

Audio Editor
------------
`Tenacity <https://nightly.link/tenacityteam/tenacity/workflows/cmake_build/master>`_

Video Editor
------------
`shotcut <https://shotcut.org/>`_ 

`Avidemux <http://avidemux.sourceforge.net/>`_

Vector Editor
-------------
`Vector Editor (inkscape) <https://inkscape.org/>`_

3D Design Tools
---------------
`Blender <https://www.blender.org/>`_ 

`SketchUp <https://www.sketchup.com/>`_

ID3 Tag Editors
---------------
`TagScanner <https://www.xdlab.ru/en/>`_

Privacy Oriented Browsers
-------------------------
`Firefox Based <https://gitlab.com/librewolf-community/browser/windows/-/releases>`_

`Chromium Based <https://ungoogled-software.github.io/ungoogled-chromium-binaries/>`_

Frontends
---------
`Playnite <https://playnite.link/>`_

`LaunchBox <https://www.launchbox-app.com/>`_

Package Managers
----------------
`Scoop <https://scoop.sh/>`_

`Chocolatey <https://chocolatey.org/>`_

`Ninite <https://ninite.com/>`_

Development Tools
-----------------
`Cygwin <https://www.cygwin.com/>`_

`MSYS2 <https://www.msys2.org/>`_

`Sourcetrail <https://github.com/CoatiSoftware/Sourcetrail>`_

Tweaks
------
`WinAero Tweaker <https://winaero.com/winaero-tweaker/>`_

`ThisIsWin11 <https://github.com/builtbybel/ThisIsWin11>`_

`BloatBox <https://github.com/builtbybel/bloatbox>`_

`Custom Live Tiles <https://www.reddit.com/r/Windows11/comments/q18ipe/live_tiles_anywhere_create_custom_live_tiles_for/>`_

`Rounded Corners Everywhere <https://github.com/torchgm/RoundedTB>`_ `Or just disable all of them <https://github.com/valinet/Win11DisableRoundedCorners>`_

`Software to tweak games for slower machines <https://www.ragnotechpowered.com/>`_

Privacy and Security
--------------------
`O&O ShutUp10++ <https://www.oo-software.com/en/shutup10>`_

`PrivateZilla <https://github.com/builtbybel/privatezilla>`_

`TinyWall <https://tinywall.pados.hu/features.php>`_

`GlassWire Firewall <https://www.glasswire.com/?cjevent=679b054f6cfb11eb83e401760a24060e>`_

`Windows 11 Debloat / Privacy Guide <https://github.com/TheWorldOfPC/Windows11-Debloat-Privacy-Guide>`_

`Optimizer <https://github.com/hellzerg/optimizer>`_

`Sophia Script <https://github.com/farag2/Sophia-Script-for-Windows>`_

Other tools
-----------
`Drag using Alt Key <https://stefansundin.github.io/altdrag/>`_

`Resize and reposition Windows <http://www.brianapps.net/sizer4/>`_

`Twinkle Tray <https://twinkletray.com/>`_

`Teracopy <https://www.codesector.com/teracopy>`_

`Microsoft PowerToys <https://docs.microsoft.com/en-us/windows/powertoys/>`_

`File Unlocker <https://lockhunter.com/>`_ `or an anlternative (IOBit Unlocker) <https://www.iobit.com/en/iobit-unlocker.php>`_

`MSMG Toolkit <https://msmgtoolkit.in>`_

`EasyUS Partition Master Free <https://www.easeus.com/partition-manager/epm-free.html>`_

`Several curated tools known as SysInternals <https://docs.microsoft.com/en-us/sysinternals/>`_

`Directory statistics (WinDirStat) <https://windirstat.net/>`_

`Automation Tool (AutoHotKey) <https://www.autohotkey.com/>`_

`Deduplication (dupeguru) <https://dupeguru.voltaicideas.net/>`_

`Veracrypt <https://veracrypt.fr>`_

Software issues
===============
`WSL2 tuning guide <https://medium.com/@lewwybogus/how-to-stop-wsl2-from-hogging-all-your-ram-with-docker-d7846b9c5b37>`_

`Windows keep adding a fucking extra keyboard <https://thegeekpage.com/fix-windows-keeps-automatically-adding-en-us-keyboard-layout-in-windows-10/>`_

`Add a shortcut key to launch terminal <https://github.com/microsoft/terminal/issues/2983>`_

`Unfuck the context menus <https://www.tomshardware.com/how-to/windows-11-classic-context-menus>`_

`Unfuck windows explorer ribbon <https://www.tomshardware.com/how-to/restore-windows-10-explorer-windows-11>`_

`Unfuck the taskbar size <https://www.tomshardware.com/how-to/change-taskbar-icon-size-windows-11>`_

`Bypass TPM requirement <https://www.tomshardware.com/how-to/bypass-windows-11-tpm-requirement>`_

Microsoft Edge Madness
----------------------
`Disable edge trying to open all links <https://www.ctrl.blog/entry/edgedeflector-default-browser.html>`_

`Edge Redirector for those microsft-edge: stupid links <https://github.com/rcmaehl/MSEdgeRedirect>`_

`Change default browser for everything <https://www.tomshardware.com/how-to/change-default-browser-windows-11>`_

Hardware issues
===============
`USB3 getting stupid <https://thegeekpage.com/fix-xhci-usb-host-controller-is-not-working-issue-in-windows-10/>`_

Other recomendations
====================
`A tool so you can write a linux/bsd to a pendrive and format this shit all together and use something better (rufus) <https://rufus.ie/en/>`_ `or an alternative (win32imager) <https://sourceforge.net/projects/win32diskimager/>`_

`A site to choose something better for your workload <https://distrowatch.com/>`_
