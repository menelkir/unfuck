UNFUCK
======

This repository have all things you need to unfuck your system.

Feel free to open an issue or a merge request if you have something useful to add.

For a windows unfuck software, here's a list_.

.. _list: Windows.rst

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
